package mySocialNetworking;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * To create a class for userCollection
 * @author swgaddam1
 *
 */

public class UserCollection {
	ArrayList<String> userList  = new ArrayList<String>();
	/**
	 * To create createNewUser method using parameters
	 * by using ArrayList and addUser
	 * @param name
	 * @param phoneNo
	 * @param password
	 * @return
	 */
	public boolean createNewUser(String name, String phoneNo, String password)
	{
		try
		{
			User addUser=new User(name, phoneNo, password);
			ArrayList<String> listOfAlluser;
			userList.addAll((Collection<? extends String>) addUser);
			return true;
		}catch(Exception e) {
		
		return false;
		
	}
}
	/**
	 * To create a method  for printAllusers 
	 * by sing Iterator to print the currentUser
	 */
	public void printAllusers() {
		for (Iterator iter = userList.iterator();
				iter.hasNext();) {
			User currentUser = (User)iter.next();
			System.out.println(currentUser);
			}
	}
}
