package mySocialNetworking;
/**
 * To create a usercollection for Whatsapp Application
 * @author swgaddam
 *
 */

public class User {
	public  String name;     //To represents the name
	public  String phoneNo;  //To represents the phoneNo
	public  String password; //To represents the password
/**
 * To create a constructor with arguments for the User class
 * @param name
 * @param phoneNo
 * @param password
 */
	public User(String name, String phoneNo,String password)
	{
		this.name=name;         //using this statement
		this.phoneNo=phoneNo;   //using this statement
		this.password=password; //using this statement
		System.out.println(this);
		
    }
	@Override
	public String toString() 
	   {
		String StringToReturn = "";
		StringToReturn += "name      :     "+this.name+                     "\n";
		StringToReturn += "phoneNo   :     "+this.phoneNo+                  "\n";
		StringToReturn += "password  :     "+this.password+                 "\n";
		return StringToReturn;
		}
}
