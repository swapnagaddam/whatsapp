package mySocialNetworking;
import java.util.ArrayList;
/**
 * To create a Message class to implement the messages
 * by using ArrayList
 * @author swgaddam1
 *
 */
public class Message {
	ArrayList<String> messageList = new ArrayList<String>();
	private String sendmessage1;
	private String recievemsg1;
	private String msgbody1;
/**
 * To create a constructor with parameters 
 * using this statement	
 * @param sendmsg
 * @param recvmsg
 * @param msgbody
 */
public Message(String sendmsg, String recvmsg, String msgbody){
	this.sendmessage1=sendmsg;  //using this statement
	this.recievemsg1=recvmsg;   //using this statement
	this.msgbody1=msgbody;      //using this statement
	System.out.println(this);
	}
/**
 * override the default toString method of Message 
 * so that message can be printed in human readable format
 */
@Override
public String toString() {
	String returnToMe=" ";
	returnToMe+="sendmsg:      "+this.sendmessage1+  "\n";
	returnToMe+="recvmsg:      "+this.recievemsg1+   "\n";
	returnToMe+="msgbody:      "+this.msgbody1+      "\n";
	return returnToMe;
}
	
}

